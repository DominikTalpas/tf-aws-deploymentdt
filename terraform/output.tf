output "instance_public_ip-master" {
  value = aws_instance.master.*.public_ip
}

output "instance_public_ip-worker" {
  value = aws_instance.worker.*.public_ip
}
